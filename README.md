# Introduction
Network note for programming starters.
![输入图片说明](image2.png)
Left：Android、iOS、Huawei HarmonyOS、Flutter、Vue、uni-app。  
Right：Java、Python、Go、PHP、NodeJS


# Preview
![输入图片说明](image.png)
# Server Features
 1. Register
 2. Login
 3. Add\Delete\Modify\Query note

# Open Source
[https://gitee.com/lblbc/cloud-note](https://gitee.com/lblbc/cloud-note)
![](https://img-blog.csdnimg.cn/b8697583eb8947b6865814f5f0eb3844.png)


# About Me
Graduated from China Xiamen University  
Ever worked for Huawei as software developer  
focus on programming learning. http://lblbc.cn/blog  
Including：Android(Java、Kotlin)、iOS(SwiftUI)、Flutter(Dart)、Window Desktop(C#)、Front End(WeChat mini-program、uni-app、vue)、Back End（Java、Kotlin、NodeJS、Python、PHP、Go、C、C++）、HarmonyOS(Huawei)  
Follow me on China wechat：蓝不蓝编程  