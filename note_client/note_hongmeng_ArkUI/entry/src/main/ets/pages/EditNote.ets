import prompt from '@ohos.prompt';
/**
 * 厦门大学计算机专业 | 华为开发专家(HDE)
 * 专注《零基础学编程系列》  http://lblbc.cn/blog
 * 包含：鸿蒙 | Java | 安卓 | 前端 | Flutter | iOS | 小程序
 * 公众号：蓝不蓝编程
 */
import router from '@ohos.router';
import { AxiosError, AxiosResponse } from '@ohos/axios';
import { Note as Note } from '../beans/Note';
import axiosInstance from '../network/HttpUtil';
import { ModifyNoteReq } from '../network/ModifyNoteReq';
import { Response } from '../network/Response';

interface RouteParams {
  noteId?: string;
}

@Entry
@Component
struct EditNote {
  @State note: Note = new Note()
  private noteId: string = '';

  aboutToAppear() {
    const params = router.getParams() as RouteParams
    this.noteId = params.noteId || ''
    this.queryData()
  }

  build() {
    Column() {
      Row() {
        Image($r("app.media.back"))
          .width(50)
          .height(50)
          .padding(10)
          .onClick(() => {
            router.back()
          })
        Text('编辑').fontSize(20)
        Blank()
        Text('保存').fontColor('#717171').fontSize(20)
          .onClick(() => {
            this.save()
          })
        Text(' | ').fontColor('#717171').fontSize(10).padding({ left: 10, right: 10 })
        Text('删除').fontColor('#717171').fontSize(20).margin({ right: 20 })
          .onClick(() => {
            this.delete()
          })
      }.width('100%').margin({ bottom: 10 })

      TextArea({ placeholder: "请输入内容", text: this.note.content })
        .width('100%')
        .height('100%')
        .backgroundColor('#FFFFFF')
        .fontSize(20)
        .margin({ top: 10, left: 20, right: 20 })
        .onChange((value) => {
          this.note.content = value
        })
    }.width('100%').height('100%')
  }

  private async queryData() {
    let url = "note/notes/" + this.noteId
    axiosInstance.get<Response<Note>>(url)
      .then((response: AxiosResponse<Response<Note>>) => {
        if (response.data.code === 0) { // 处理成功情况
          console.info("Categories: ", response.data.data);
          this.note = response.data.data;
        } else {
          console.error('请求失败，消息：' + response.data.msg);
        }
      })
      .catch((error: AxiosError) => { // 处理错误情况
        console.error('请求出错：', error);
      })
  }

  private async save() {
    let url = "note/notes/" + this.noteId
    let params = new ModifyNoteReq()
    params.content = this.note.content

    axiosInstance.put<Response<string>>(url, params)
      .then((response: AxiosResponse<Response<string>>) => {
        if (response.data.code === 0) { // 处理成功情况
          router.back()
        } else {
          console.error('请求失败，消息：' + response.data.msg);
        }
      })
      .catch((error: AxiosError) => { // 处理错误情况
        console.error('请求出错：', error);
      })
  }

  private async delete() {
    let url = "note/notes/" + this.noteId
    axiosInstance.delete<Response<string>>(url)
      .then((response: AxiosResponse<Response<string>>) => {
        if (response.data.code === 0) { // 处理成功情况
          router.back()
        } else {
          console.error('请求失败，消息：' + response.data.msg);
        }
      })
      .catch((error: AxiosError) => { // 处理错误情况
        console.error('请求出错：', error);
      })
  }
}