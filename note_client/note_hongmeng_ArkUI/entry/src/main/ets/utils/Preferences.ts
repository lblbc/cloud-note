/**
 * 厦门大学计算机专业 | 华为开发专家(HDE)
 * 专注《零基础学编程系列》  http://lblbc.cn/blog
 * 包含：鸿蒙 | Java | 安卓 | 前端 | Flutter | iOS | 小程序
 * 公众号：蓝不蓝编程
 */
import dataPreferences from '@ohos.data.preferences';
import { Context } from '@kit.AbilityKit';

const PREFERENCES_NAME = "lblbc"
const KEY_USER_TOKEN = "user_info";

export default class Preferences {
  private static instance: Preferences;
  private preferences: dataPreferences.Preferences;

  private constructor(context: Context) {
    this.preferences = dataPreferences.getPreferencesSync(context, {
      name: PREFERENCES_NAME
    });
  }

  getToken(): string | undefined {
    return this.preferences.getSync(KEY_USER_TOKEN, "").toString();
  }

  saveToken(token: string) {
    this.preferences.putSync(KEY_USER_TOKEN, token);
    this.preferences.flush();
  }

  static get(context: Context): Preferences {
    if (Preferences.instance === undefined) {
      Preferences.instance = new Preferences(context);
    }
    return Preferences.instance;
  }
}